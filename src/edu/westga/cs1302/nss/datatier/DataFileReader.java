package edu.westga.cs1302.nss.datatier;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Scanner;

import edu.westga.cs1302.music.model.Song;
import edu.westga.cs1302.nss.model.Earthquake;
import edu.westga.cs1302.nss.model.Network;
import edu.westga.cs1302.nss.model.Station;
import edu.westga.cs1302.nss.resources.UI.ExceptionMessages;

/**
 * Reads seismic data from a file with extension Constants.FILE_EXTENSION which
 * is a CSV file with the following format:
 * station,year,month,day,hour,minute,second,location,magnitude,significance,distance,depth
 * 
 * @author CS1302
 * @version Fall 2021
 */
public class DataFileReader {

	private File seismicDataFile;

	/**
	 * Instantiates a new seismic data file reader.
	 *
	 * @precondition seismicDataFile != null
	 * @postcondition none
	 * @param seismicDataFile the seismic data file
	 */
	public DataFileReader(File seismicDataFile) {
		if (seismicDataFile == null) {
			throw new IllegalArgumentException(ExceptionMessages.FILE_CANNOT_BE_NULL);
		}
		this.seismicDataFile = seismicDataFile;
	}

	/**
	 * Reads all the earthquake data from the file one line at a time where each
	 * line contains information about the earthquake as well as the station where
	 * it was recorded. Parses each line and creates an earthquake object and stores
	 * it in the corresponding station of the passed in network.
	 * 
	 * @precondition network != null
	 * @postcondition none
	 * @param network the network to load from file
	 * @return the list of names of all newly added stations
	 * @throws FileNotFoundException exception to be thrown if file is not found
	 */
	public ArrayList<String> loadAllEarthquakesToStations(Network network) throws FileNotFoundException {
		ArrayList<String> allStations = new ArrayList<String>();
		try (Scanner aScanner = new Scanner(this.seismicDataFile))	{
			while (aScanner.hasNextLine()) {
				String earthquakeInfo = aScanner.nextLine();
				String stationName = "";
				int aYear = 0;
				int aMonth = 0;
				int aDay = 0;
				int anHour = 0;
				int aMinute = 0;
				int aSecond = 0;
				String aLocation = "";
				double aMagnitude  = 0;
				int aSignificance = 0;
				double aDistance = 0;
				double aDepth = 0;
				String delims = "[,]";
				String[] splitEarthquakeInfo = earthquakeInfo.split(delims);
				for (int curr = 0; curr < 14; curr++) {
					if (curr == 0) {
						stationName = splitEarthquakeInfo[0];
					}
					if (curr == 1) {
						aYear = Integer.parseInt(splitEarthquakeInfo[1]);
					}
					if (curr == 2) {
						aMonth = Integer.parseInt(splitEarthquakeInfo[2]);
					}
					if (curr == 3) {
						aDay = Integer.parseInt(splitEarthquakeInfo[3]);
					}
					if (curr == 4) {
						anHour = Integer.parseInt(splitEarthquakeInfo[4]);
					}
					if (curr == 5) {
						aMinute = Integer.parseInt(splitEarthquakeInfo[5]);
					}
					if (curr == 6) {
						aSecond = Integer.parseInt(splitEarthquakeInfo[6]);
					}
					if (curr == 7) {
						aLocation = splitEarthquakeInfo[7];
					}
					if (curr == 8) {
						aMagnitude = Double.parseDouble(splitEarthquakeInfo[8]);
					}
					if (curr == 9) {
						aSignificance = Integer.parseInt(splitEarthquakeInfo[9]);
					} 
					if (curr == 10) {
						aDistance = Double.parseDouble(splitEarthquakeInfo[10]);
					}
					if (curr == 11) {
						aDepth = Double.parseDouble(splitEarthquakeInfo[11]);
					}
				}
				LocalDateTime aDate = LocalDateTime.of(aYear, aMonth, aDay, anHour, aMinute, aSecond);
				Earthquake anEarthquake = new Earthquake(aDate, aLocation, aMagnitude, aSignificance, aDistance, aDepth);
				Station aStation = new Station(stationName);
				aStation.getSeismicData().add(anEarthquake);
				allStations.add(aStation.getName());
			}
		} catch (IllegalArgumentException anException) {	
		}
		return allStations;
	}
}
