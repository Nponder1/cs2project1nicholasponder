package edu.westga.cs1302.nss.model.test;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.nss.model.Earthquake;
import edu.westga.cs1302.nss.model.SeismicData;

class TestSeismicDataAddMethod {

	@Test
	void testAddMultipleEarthquakes() {
		SeismicData aSetOfData = new SeismicData();
		Earthquake earthquake0 = new Earthquake(LocalDateTime.of(2000, 1, 1, 1, 1, 1), "Carrollton", 1.0, 1, 5.0, 1.0);
		Earthquake earthquake1 = new Earthquake(LocalDateTime.of(2001, 1, 1, 1, 1, 1), "Carrollton", 1.0, 1, 5.0, 1.0);
		Earthquake earthquake2 = new Earthquake(LocalDateTime.of(1999, 1, 1, 1, 1, 1), "Carrollton", 1.0, 1, 5.0, 1.0);
		
		aSetOfData.add(earthquake0);
		aSetOfData.add(earthquake1);
		aSetOfData.add(earthquake2);
		
		assertEquals(3, aSetOfData.size());
	}
	
	@Test
	void testAddTheSameEarthquake() {
		SeismicData aSetOfData = new SeismicData();
		Earthquake earthquake0 = new Earthquake(LocalDateTime.of(1999, 1, 1, 1, 1, 1), "Carrollton", 1.0, 1, 5.0, 1.0);
		Earthquake earthquake1 = new Earthquake(LocalDateTime.of(1999, 1, 1, 1, 1, 1), "Carrollton", 1.0, 1, 5.0, 1.0);
		
		aSetOfData.add(earthquake0);
		assertThrows(IllegalArgumentException.class, ()-> aSetOfData.add(earthquake1));
	}

}
